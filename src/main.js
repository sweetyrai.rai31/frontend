import Vue from 'vue'
import App from './App.vue'
import {
  BootstrapVue,
  IconsPlugin
} from 'bootstrap-vue'
import VueRouter from 'vue-router';
import axios from 'axios';
import Vuex from 'vuex';
import home from './views/home';
import store from './views/store';
import user from './views/user';
import cart from './views/cart';
import register from './views/register';
import login from './views/login';
import recipes from './views/recipes';
import checkout from './views/checkout';
import classes from './views/classes';
import ask_your_question from './views/ask_your_question';
import dietitian_and_nutritionist from './views/dietitian_and_nutritionist';
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue);
Vue.use(IconsPlugin)
Vue.use(VueRouter);
Vue.use(Vuex);
Vue.prototype.axios = axios;


const routes = [{
    path: '/',
    name: 'home',
    component: home
  },
  {
    path: '/store',
    name: 'store',
    component: store
  },
  {
    path: '/checkout',
    name: 'checkout',
    component: checkout
  },
  {
    path: '/user',
    name: 'user',
    component: user
  },
  {
    path: '/cart',
    name: 'cart',
    component: cart
  },
  {
    path: '/classes',
    name: 'classes',
    component: classes
  },
  {
    path: '/register',
    name: 'register',
    component: register
  },
  {
    path: '/login',
    name: 'login',
    component: login
  },
  {
    path: '/ask_your_question',
    name: 'ask_your_question',
    component: ask_your_question
  },
  {
    path: '/dietitian_and_nutritionist',
    name: 'dietitian_and_nutritionist',
    component: dietitian_and_nutritionist
  },
  {
    path: '/recipes',
    name: 'recipes',
    component: recipes
  },
]

export const router = new VueRouter({
  mode: 'history',
  routes
});
Vue.prototype.routes = router;

Vue.config.productionTip = false

export const stores = new Vuex.Store({
  state : {
    products : [
      // {name : 'banana', price : 200},
      // {name : 'apple', price : 100}
    ],
    total_price : 0.00,
    quantity : 0,
    category : '',
    user : {},
    token : ''
  },
  getters: {
    products: (state) => {
      return state.products
    },
    total_price: (state) => {
      return state.total_price
    },
    
  },
  plugins: [],
  mutations: {
    SET_PRODUCTS: (state, newValue) => {
      // state.products = newValue
      state.products.push(newValue)
    },
    REMOVE_ALL_PRODUCT: (state, newValue) => {
      state.products = []
    },
    SET_TOTAL_PRICE: (state, newValue) => {
      state.total_price = newValue
    },
    SET_USER: (state, newValue) => {
      state.user = newValue
    },
    SET_TOKEN: (state, newValue) => {
      state.token = newValue
    },
    REMOVE_PRODUCT : (state, newValue) => {
      
      for( var i = 0; i < state.products.length; i++){ 
        if ( state.products[i].id === newValue.id) {
          state.products.splice(i, 1); 
        }
     }
    },
    SET_QUANTITY: (state) => {
      
      if(state.products.length > 0) {
        let count = 0
        state.products.forEach(element => {
          count = count + element.count
        })
        state.quantity = count
      }
      else {
        state.quantity = 0
      }
      
    },
    SET_COUNT :  (state, newValue) => {
      state.products.forEach(element => {
        console.log(newValue)
        if(element.id == newValue.id) {
          element.count = newValue.count
        }
      })
    },
    SET_CATEGORY :  (state, newValue) => {
      state.category = newValue
    }
  },
  actions: {
    setProducts: ({commit, state}, newValue) => {
      commit('SET_PRODUCTS', newValue)
      return state.products
    },
    setTotalPrice: ({commit, state}, newValue) => {
      commit('SET_TOTAL_PRICE', newValue)
      return state.total_price
    },
    removeProduct: ({commit, state}, newValue) => {
      commit('REMOVE_PRODUCT', newValue)
      return state.products
    },
    removeAllProduct: ({commit, state}) => {
      commit('REMOVE_ALL_PRODUCT')
      return state.products
    },
    setQuantity : ({commit, state}) => {
      commit('SET_QUANTITY')
      return state.quantity
    },
    setCount : ({commit, state}, newValue) => {
      commit('SET_COUNT', newValue)
      return state.products
    },
    setCategory: ({commit, state}, newValue) => {
      commit('SET_CATEGORY', newValue)
      return state.category
    },
    setUser: ({commit, state}, newValue) => {
      commit('SET_USER', newValue)
      return state.user
    },
    setToken: ({commit, state}, newValue) => {
      commit('SET_TOKEN', newValue)
      return state.token
    }
  }
})

new Vue({
  router,
  store : stores,
  render: h => h(App),
}).$mount('#app')